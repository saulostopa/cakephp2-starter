<div class="error-box">
	<div class="error-body text-center">
		<h1>500</h1>
		<h3 class="text-uppercase">Erro Interno do Servidor</h3>
		<p class="text-muted m-t-30 m-b-30">Tente novamente ou contate o administrador</p>
		<h6><small><?php echo $message; ?></small></h6>
		<br>
		<a href="/" class="btn btn-info btn-rounded waves-effect waves-light m-b-40">Voltar Para o Início</a>
	</div>
</div>
