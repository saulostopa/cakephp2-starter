<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" sizes="16x16" href="/plugins/images/favicon.png">
	<title>Elite Admin - is a responsive admin template</title>
	<!-- Bootstrap Core CSS -->
	<link href="/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Menu CSS -->
	<link href="/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
	<!-- toast CSS -->
	<link href="/plugins/bower_components/toast-master//css/jquery.toast.css" rel="stylesheet">
	<!-- morris CSS -->
	<link href="/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
	<!-- animation CSS -->
	<link href="/css/animate.css" rel="stylesheet">
	<link href="/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
	<!-- Custom CSS -->
	<link href="/css/style.css" rel="stylesheet">
	<!-- Custom SS -->
	<link href="/css/style_ss.css" rel="stylesheet">
	<!-- color CSS -->
	<link href="/css/colors/blue.css" id="theme"  rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-19175540-9', 'auto');
		ga('send', 'pageview');

	</script>
</head>
<body>
<?php echo $this->Flash->render(); ?>
<!-- Preloader -->
<div class="preloader">
	<div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">

	<!-- Page Content -->
	<div id="page-wrapper">
		<?php echo $this->fetch('content'); ?>
		<footer class="footer text-center"> <?php echo date('Y') ?> &copy; <?php echo Configure::read('BRAND') .' '. Configure::read('BRAND_SLOGAN') ?> </footer>
	</div>
	<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="/js/waves.js"></script>
<!--Counter js -->
<script src="/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="/plugins/bower_components/counterup/jquery.counterup.min.js"></script>

<?php if ( $this->params['controller'] == "pages" && $this->params['action'] == "index" ) : ?>
	<!--Morris JavaScript -->
	<script src="/plugins/bower_components/raphael/raphael-min.js"></script>
	<script src="/plugins/bower_components/morrisjs/morris.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="/js/dashboard1.js"></script>
<?php endif; ?>

<!-- Sweet-Alert  -->
<script src="/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>

<script src="/js/custom.min.js"></script>
<script src="/js/validator.js"></script>

<!-- Sparkline chart JavaScript -->
<script src="/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

<script type="text/javascript">
	jQuery(document).ready(function() {
		// hide box alert
		var sec = 3500;
		setTimeout(function()
		{
			$("#flashMessage").hide('blind');
		}, sec);

		$(".myadmin-alert .closed").click(function(event){
			$(this).parents(".myadmin-alert").fadeToggle(350);
			return false;
		});

		$(".myadmin-alert-click").click(function(event){
			$(this).fadeToggle(350);
			return false;
		});
	});
</script>

</body>
</html>
