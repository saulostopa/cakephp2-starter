    <!-- Top Nav -->
    <header id="topNav" class="<?php echo ($this->name == "Pages" && $this->view == "index") ? "fixed" : "" ?>">
        <div class="container">
            <!-- Logo -->
            <a class="logo onepage" href="/">
                <img src="/theme/LandingPage/assets/images/logo.png" height="50" alt="" />
            </a>

            <!-- Mobile Menu Button -->
            <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
                <i class="fa fa-bars"></i>
            </button>

            <!-- Top Nav -->
            <div class="navbar-collapse nav-main-collapse collapse pull-right">
                <nav class="nav-main">

                    <?php if ($this->name == "Pages" && $this->view == "index") { ?>
                    <ul id="topMain" class="nav nav-pills nav-main">
                        <li><a class="scrollTo" href="#wrapper">INÍCIO <span>bem-vindo</span></a></li>
                        <li><a class="scrollTo" href="#about">SOBRE <span>sobre nós</span></a></li>
                        <li><a class="scrollTo" href="#work">O QUE FAZEMOS <span>o projeto</span></a></li>
                        <li><a class="scrollTo" href="#services">PARCEIROS <span>nossos parceiros</span></a></li>
                        <li><a class="scrollTo" href="#contact">CONTATO <span>fale conosco</span></a></li>
                        <li><a href="/login">LOGIN <span>entrar no sistema</span></a></li>
                    </ul>
                    <?php }else{ ?>
                    <ul id="topMain" class="nav nav-pills nav-main">
                        <li><a class="" href="/#wrapper">INÍCIO <span>bem-vindo</span></a></li>
                        <li><a class="" href="/#about">SOBRE <span>sobre nós</span></a></li>
                        <li><a class="" href="/#work">O QUE FAZEMOS <span>o projeto</span></a></li>
                        <li><a class="" href="/#services">PARCEIROS <span>nossos parceiros</span></a></li>
                        <li><a class="" href="/#contact">CONTATO <span>fale conosco</span></a></li>
                        <li><a href="/login">LOGIN <span>entrar no sistema</span></a></li>
                    </ul>
                    <?php } ?>
                </nav>
            </div>
            <!-- /Top Nav -->

        </div><!-- /.container -->
    </header>
    <!-- /Top Nav -->