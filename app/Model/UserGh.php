<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
class User extends AppModel
{

    public $belongsTo   = array('Group');
    public $actsAs      = array('Acl' => array('type' => 'requester', 'enabled' => false));
    public $validate    = array
    (
        'email' => array
        (
            'required' => array(
                'rule' => 'notBlank'
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'required' => 'create'
            ),
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank'
            )
        )
    );

    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['User']['group_id'])) {
            $groupId = $this->data['User']['group_id'];
        } else {
            $groupId = $this->field('group_id');
        }
        if (!$groupId) {
            return null;
        }
        return array('Group' => array('id' => $groupId));
    }

    public function bindNode($user) {
        return array('model' => 'Group', 'foreign_key' => $user['User']['group_id']);
    }

    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }

    public function isOwnedBy($user)
    {
        return $this->field('id', array('id' => $user));
    }

}