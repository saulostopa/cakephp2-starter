<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Epona - Multipurpose Template</title>
	<meta name="keywords" content="HTML5,CSS3,Template" />
	<meta name="description" content="" />
	<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<!-- mobile settings -->
	<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

	<!-- Favicon -->
	<link rel="shortcut icon" href="/assets/images/demo/favicon.ico" />

	<!-- WEB FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800%7CDosis:300,400&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />

	<!-- CORE CSS -->
	<link href="/theme/LandingPage/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="/theme/LandingPage/assets/css/font-awesome.css" rel="stylesheet" type="text/css" />
	<link href="/theme/LandingPage/assets/css/sky-forms.css" rel="stylesheet" type="text/css" />
	<link href="/theme/LandingPage/assets/css/weather-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="/theme/LandingPage/assets/css/line-icons.css" rel="stylesheet" type="text/css" />
	<link href="/theme/LandingPage/assets/plugins/owl-carousel/owl.pack.css" rel="stylesheet" type="text/css" />
	<link href="/theme/LandingPage/assets/plugins/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
	<link href="/theme/LandingPage/assets/css/animate.css" rel="stylesheet" type="text/css" />
	<link href="/theme/LandingPage/assets/css/flexslider.css" rel="stylesheet" type="text/css" />

	<!-- REVOLUTION SLIDER -->
	<link href="/theme/LandingPage/assets/css/revolution-slider.css" rel="stylesheet" type="text/css" />
	<link href="/theme/LandingPage/assets/css/layerslider.css" rel="stylesheet" type="text/css" />

	<!-- THEME CSS -->
	<link href="/theme/LandingPage/assets/css/essentials.css" rel="stylesheet" type="text/css" />
	<link href="/theme/LandingPage/assets/css/layout.css" rel="stylesheet" type="text/css" />
	<link href="/theme/LandingPage/assets/css/header-default.css" rel="stylesheet" type="text/css" />
	<link href="/theme/LandingPage/assets/css/footer-default.css" rel="stylesheet" type="text/css" />
	<link href="/theme/LandingPage/assets/css/color_scheme/red.css" rel="stylesheet" type="text/css" id="color_scheme" />

	<!-- Morenizr -->
	<script type="text/javascript" src="/theme/LandingPage/assets/plugins/modernizr.min.js"></script>

	<!--[if lte IE 8]>
	<script src="/theme/LandingPage/assets/plugins/respond.js"></script>
	<![endif]-->

	<!-- JAVASCRIPT FILES -->
	<script type="text/javascript" src="/theme/LandingPage/assets/plugins/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="/theme/LandingPage/assets/plugins/jquery.isotope.js"></script>
	<script type="text/javascript" src="/theme/LandingPage/assets/plugins/masonry.js"></script>

	<script type="text/javascript" src="/theme/LandingPage/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/theme/LandingPage/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="/theme/LandingPage/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
	<script type="text/javascript" src="/theme/LandingPage/assets/plugins/knob/js/jquery.knob.js"></script>
	<script type="text/javascript" src="/theme/LandingPage/assets/plugins/flexslider/jquery.flexslider-min.js"></script>

	<!-- REVOLUTION SLIDER -->
	<script type="text/javascript" src="/theme/LandingPage/assets/plugins/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="/theme/LandingPage/assets/plugins/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="/theme/LandingPage/assets/js/revolution_slider.js"></script>

	<script type="text/javascript" src="/theme/LandingPage/assets/js/scripts.js"></script>

</head>

<!--
    Available body classes:
        smoothscroll			= enable chrome browser smooth scroll
        grey 					= grey content background
        boxed 					= boxed style
        pattern1 ... pattern10 	= background pattern

    Background Image - add to body:
        data-background="assets/images/boxed_background/1.jpg"

    Examples:
        <body class="smoothscroll grey boxed pattern3">
        <body class="smoothscroll boxed" data-background="assets/images/boxed_background/1.jpg">
-->
<body class="smoothscroll">

<div id="wrapper">

	<?php echo $this->element('header'); ?>
	<?php echo $this->fetch('content'); ?>
	<?php echo $this->element('footer'); ?>


</div><!-- /#wrapper -->

</body>
</html>