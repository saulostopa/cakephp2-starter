<?php

App::uses('CakeEmail', 	'Network/Email');
App::uses('Controller', 'Controller');

class AppController extends Controller {

    public $components = array
    (
        'Flash',
        'Session',
        'RequestHandler',
        'Paginator',
        'DebugKit.Toolbar',
        'Acl',
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'pages',
                'action' => 'index'
            ),
            'logoutRedirect' => '/'
            ,'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish',
                    'fields' => array('username' => 'email')
                )
            )
            ,'authorize' => array('Controller')
        )
    );

    function beforeFilter()
    {
        // Get Domain name to set Brand
        //$main_domain = $this->getNameFromSubDomain($_SERVER["SERVER_NAME"]);
        $server_name = $_SERVER["SERVER_NAME"];
        $full_domain = $this->get_main_domain("http://" . $server_name);
        $brand       = ClassRegistry::init('Setting')->findByBrandName('B2S');

        if ( $server_name == 'local.cakephp2.saulostopa.com' ) // dev
        {
            $host = 'local.cakephp2.saulostopa.com';
        }
        elseif ( $server_name == 'hml.aveeze.saulostopa.com' ) // homolog
        {
            $host = 'hml.cakephp2.saulostopa.com';
        }
        else
        {
            $host = ''; // prod
        }

        if ( isset( $_SERVER["HTTPS"] ) && strtolower( $_SERVER["HTTPS"] ) == "on" ) // ToDo: https://book.cakephp.org/2.0/en/core-libraries/components/security-component.html
            $scheme = 'https://';
        else {
            $scheme = 'http://';
        }

        Configure::write(array
            (
                 'BRAND'            => (!empty($brand["Setting"]["brand_name"])  ? $brand["Setting"]["brand_name"]  : "YourBrand")
                ,'BRAND_SLOGAN'     => (!empty($brand["Setting"]["brand_slogan"])? $brand["Setting"]["brand_slogan"]: "Your Slogan")
                ,'HOST'          	=> $scheme.$host
                ,'HOST_SUFIX'      	=> (!empty($brand["Setting"]["host_sufix"])  ? $brand["Setting"]["host_sufix"]  : $full_domain)
                ,'DIR_NAME'         => (!empty($brand["Setting"]["dir_name"])    ? $brand["Setting"]["dir_name"]    : "yourbrand")
                ,'LOGO_NAME'     	=> (!empty($brand["Setting"]["logo_name"])   ? $brand["Setting"]["logo_name"]   : "logo.png")
                ,'AVATAR_NAME'     	=> (!empty($brand["Setting"]["avatar_name"]) ? $brand["Setting"]["avatar_name"] : "avatar.jpg")
                ,'FULL_PATH'     	=> (!empty($brand["Setting"]["full_path"])   ? $brand["Setting"]["full_path"]   : "/img/")
            )
        );

        $payments['pagseguro_sdx_api_email'] 							= 'contato.aveeze@gmail.com';
        $payments['pagseguro_sdx_api_token']							= '97403AD9DFA34CAEBEA03BA74ABF9127';

        $payments['pagseguro_sdx_comprador_passwd']						= 'R2055707A21c0H99';
        $payments['pagseguro_sdx_comprador_email']       				= 'c61519842631088657584@sandbox.pagseguro.com.br';

        $payments['pagseguro_prd_api_email'] 						    = '';
        $payments['pagseguro_prd_api_token']					 		= '';

        $payments['pagseguro_sdx_url_js_checkout_transparent']          = 'stc.sandbox.pagseguro.uol.com.br';
        $payments['pagseguro_sdx_url_ws_transaction_and_signature']     = 'ws.sandbox.pagseguro.uol.com.br';
        $payments['pagseguro_sdx_url_request_pre_approval_signature']   = 'sandbox.pagseguro.uol.com.br';

        if ( $server_name == 'local.aveeze.com.br' || $server_name == 'hml.aveeze.saulostopa.com' )
        {
            Configure::write(array
                (
                    'PAGSEGURO_EMAIL'                                      => $payments['pagseguro_sdx_api_email']
                ,'PAGSEGURO_TOKEN'                                      => $payments['pagseguro_sdx_api_token']
                ,'PAGSEGURO_SENDER_EMAIL_SANDBOX'                       => $payments['pagseguro_sdx_comprador_email']
                ,'PAGSEGURO_URL_JS_CHECKOUT_TRANSPARENT'                => $payments['pagseguro_sdx_url_js_checkout_transparent']
                ,'PAGSEGURO_URL_TRANSACTION_SIGNATURE'                  => $payments['pagseguro_sdx_url_ws_transaction_and_signature']
                ,'PAGSEGURO_URL_REQUEST_PRE_APPROVAL_SIGNATURE'         => $payments['pagseguro_sdx_url_request_pre_approval_signature']

                ,'SERASA_URL_REST'                                      => "http://www.soawebservices.com.br/restservices/test-drive"
                ,'SERASA_URL_SOA'                                       => "http://www.soawebservices.com.br/webservices/test-drive"
                ,'SERASA_EMAIL'                                         => "contato.aveeze@gmail.com"
                ,'SERASA_SENHA'                                         => "6at3EyHIf"
                ,'SERASA_ENV'                                           => "HML"
                ,'ENV'                                                  => "HML"
                )
            );
        }
        else
        {
            Configure::write(array
                (
                    'PAGSEGURO_EMAIL'                                      => $payments['pagseguro_sdx_api_email']
                ,'PAGSEGURO_TOKEN'                                      => $payments['pagseguro_sdx_api_token']
                ,'PAGSEGURO_SENDER_EMAIL_SANDBOX'                       => ''
                ,'PAGSEGURO_URL_JS_CHECKOUT_TRANSPARENT'                => $payments['pagseguro_sdx_url_js_checkout_transparent']
                ,'PAGSEGURO_URL_TRANSACTION_SIGNATURE'                  => $payments['pagseguro_sdx_url_ws_transaction_and_signature']
                ,'PAGSEGURO_URL_REQUEST_PRE_APPROVAL_SIGNATURE'         => $payments['pagseguro_sdx_url_request_pre_approval_signature']

                ,'SERASA_URL_REST'                                      => "http://www.soawebservices.com.br/restservices/"
                ,'SERASA_URL_SOA'                                       => "http://www.soawebservices.com.br/webservices/"
                ,'SERASA_EMAIL'                                         => "contato.aveeze@gmail.com"
                ,'SERASA_SENHA'                                         => "6at3EyHIf"
                ,'SERASA_ENV'                                           => "PRD"
                ,'ENV'                                                  => "PRD"
                )
            );
        }

        if ( $this->RequestHandler->isMobile() ) {/*$this->theme = 'Mobile';*/}

        if ( isset($this->request->params['ext']) && $this->request->params['ext'] == 'json' ) // request from ajax/API
        {
            if ( $this->request->params['action'] == 'login' || $this->request->params['action'] == 'logout' )
                return true;

            if ( ! $this->checkApiAccess() )
                throw new MethodNotAllowedException(__('Requisição Não Autorizada!')); // 401 => https://book.cakephp.org/2.0/en/development/exceptions.html#using-appcontroller-apperror

            $this->Auth->allow('add','view','edit','delete','index');
        }

        if ( isset($this->request->params['action']) && $this->request->params['action'] == 'admin_login' )
            $this->redirect('/login');
    }

    public function checkApiAccess()
    {
        $token_sender  = $this->request->header('Api-Token');
        $token_session = CakeSession::read('User-Token');

        if ($token_sender === $token_session)
        {
            // Verify if request is mobile => http://book.cakephp.org/3.0/en/controllers/components/request-handling.html#obtaining-request-information
            if ( $this->RequestHandler->isMobile() ) {
                // Verify if security key application is = key token application allowed for right mobile app
                if ( Configure::read('Security.salt') ==  $this->request->header('App-Token') ) {
                    //$this->Auth->allow(['index']);
                    return true;
                }
            } else {
                //$this->Auth->allow(['index']);
                return true;
            }
        }
        return false;
    }

    public function isAuthorized($user = null)
    {
        // Any registered user can access public functions
        if (empty($this->request->params['admin'])) {
            return true;
        }

        // Only admins can access admin functions
        if (isset($this->request->params['admin'])) {
            return (bool)($user['role'] === 'admin');
        }

        // Default deny
        return false;
    }

    public function get_main_domain($url)
    {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : '';
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return null;
    }

    public function getNameFromSubDomain($url)
    {
        $tmp = explode('.', $url);
        $subdomain = next($tmp);
        return $subdomain;
    }
}
