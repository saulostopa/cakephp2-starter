-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: cakephp_2_9_5
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '1',
  `email` varchar(80) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `role` varchar(20) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `cpf` char(14) NOT NULL,
  `full_path_files` varchar(200) DEFAULT NULL,
  `token` varchar(256) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `occupation` char(120) DEFAULT NULL,
  `zipcode` char(8) DEFAULT NULL,
  `address` varchar(120) DEFAULT NULL,
  `number` char(10) DEFAULT NULL,
  `complement` char(40) DEFAULT NULL,
  `neighborhood` char(120) DEFAULT NULL,
  `city` char(120) DEFAULT NULL,
  `state` char(2) DEFAULT NULL,
  `cellphone` char(11) DEFAULT NULL,
  `phone` char(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `parent_id`, `email`, `password`, `group_id`, `role`, `first_name`, `last_name`, `cpf`, `full_path_files`, `token`, `created`, `modified`, `gender`, `birthday`, `occupation`, `zipcode`, `address`, `number`, `complement`, `neighborhood`, `city`, `state`, `cellphone`, `phone`) VALUES (1,1,'admin@aveeze.com.br','$2a$10$CTkfFfd8iby6f8CZyStdy.74RVLT9Gw4DCi6VivZSS6AMhxdgyIc.',1,'admin','Aveeze','Notificações Online','','c4ca4238a0b923820dcc509a6f75849b',NULL,'2017-09-26 12:39:31','2017-10-10 11:47:36',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,1,'saulostopa@gmail.com','$2a$10$W4AL04UvdC9leCE/ZBcMfuUwkuPvudOnh88JzwNrYMLPY5hj0Yima',1,'admin','Saulo','Stopa','27195027821',NULL,'2e51ba3bf6fa092af72256056ff2cfb047fd0b4f','2017-09-26 12:39:31','2018-09-28 12:24:54',0,'2018-09-28','Comercial','38408186','Avenida Francisco Ribeiro','11','B2','Santa Mônica','Uberlândia','MG','34555555550','3344334444'),(3,1,'pedro@ricardo.com.br','$2a$10$8Hx7rs.KCEkg7gZSRNlYk.2.RQD7bli/Atpt5uloqs1WgnEmfUKtS',2,'manager','Pedro','Ricardo','',NULL,NULL,'2017-10-02 20:41:17','2017-10-02 22:39:51',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,3,'carlos@chagas.com.br','$2a$10$l6FR3kzBhEoViWqZEx/DE.6nBgtyuVJK8.bCzuHIYe.BFAyZh.X6a',3,'user','Carlos','Chagas','',NULL,NULL,'2017-10-09 19:16:28','2017-10-09 19:16:28',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,3,'danilo@elfort.com.br','$2a$10$MPA8xq0Zl3CN2npUeshYfumxvkKSd9N5jz/fuBT1aNqvCgb98AyfS',3,'user','Danilo','Elfort','',NULL,NULL,'2017-10-09 19:17:51','2017-10-09 19:17:51',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,3,'eliseu@almeida.com.br','$2a$10$JISw2800acytYlcjwg/I8eXJ3OI3X/EYjbKIW.pYk5QvQrVXLsYdS',2,'manager','Eliseu','Almeida','',NULL,NULL,'2017-10-09 19:18:14','2017-10-09 19:18:14',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,3,'fabio@goncalves.com.br','$2a$10$tbS25XxXR/2z0OoDXSJ5O.NCUVZMNPcGDQ3xLrLkd1xDZ6AkrPOZe',3,'user','Fabio','Gonçalves','',NULL,NULL,'2017-10-09 19:19:15','2017-10-09 19:19:15',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,3,'gabriel@hector.com.br','$2a$10$tzbiw.PnQLsO/XfQY3T4quceFO8E7wVXkInnYNrOdHlA06wJcHF6a',3,'user','Gabriel','Hector','',NULL,NULL,'2017-10-09 19:21:24','2017-10-09 19:21:24',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,3,'itamar@franco.com.br','$2a$10$tB99cTFQyDHZJ.d/denUheKidqCqA9wA8fK2sxMpXyArCENo.6Kta',3,'user','Itamar','Franco','',NULL,NULL,'2017-10-09 19:22:05','2017-10-09 19:22:05',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,3,'julio@brandao.com.br','$2a$10$6UagoQaYAVxzuxuG/MhAROdsDrh8HzapxjHgRYB5MuwC44XAnsVs6',3,'user','Julio','Brandão','',NULL,NULL,'2017-10-09 19:22:28','2017-10-09 19:22:28',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,1,'kleber@limeira.com.br','$2a$10$jmYRl6QghA5QqVEc6diAweE1nagJv9pbnUvs2XQA2lGytTswjXWli',3,'user','Kleber','Limeira','',NULL,NULL,'2017-10-09 19:23:01','2017-10-09 19:23:01',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,1,'luciano@mendes.com.br','$2a$10$G0egisr8wSWm26YF9WRkBONX24/2LzAOfom/oW4f9m1/zprYqxAgG',3,'user','Luciano','Mendes','',NULL,NULL,'2017-10-09 19:23:32','2017-10-09 19:23:32',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-29  8:31:32
