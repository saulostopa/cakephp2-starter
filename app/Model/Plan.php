<?php

class Plan extends AppModel {

    public $hasMany = array(
        'PlanUsers'    => array(
            'className'     => 'PlanUsers',
            'foreignKey'    => 'plan_id',
            'dependent'     => false
        )
    );

}