<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Contratos</h4>
		</div>
		<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			<?php echo $this->Html->link('Novo', array('controller' => 'agreements', 'action' => 'add'), array('class' => 'btn btn-info pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light')); ?>
			<ol class="breadcrumb">
				<li><a href="/" title="Voltar para a Home">Início</a></li>
				<li><a href="/contratos" title="Listar todos">Contratos</a></li>
			</ol>
		</div>
	</div>
	<!-- /row -->

	<div class="row">
		<div class="col-lg-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Contratos de Clientes</h3>
				<p class="text-muted m-b-20">Listagem de todos os contratos cadastrados no sistema | Total: <?php echo $numbers ?></p>
				<table class="tablesaw table-striped table-hover table-bordered table" data-tablesaw-mode="columntoggle">
					<thead>
					<tr>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1" data-tablesaw-sortable-default-col>Id</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Nome</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">CPF/CNPJ</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Valor</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Tipo</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Vigência</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Garantia</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Público</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Data</th>
						<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8" class="text-center fit" title="Editar"></th>
						<!--<th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="9" class="text-center fit" title="Deletar">Del.</th>-->
					</tr>
					</thead>
					<tbody>

					<?php foreach ($result as $item): ?>
					<tr id="<?php echo $item['Agreement']['id'] ?>">
						<td><?php echo $item['Agreement']['id'] ?></td>
						<td><?php echo $item['Agreement']['name'] ?></td>

						<td><?php echo $this->Custom->mask_cpf_cnpj($item['Agreement']['cpf_cnpj']) ?></td>
						<td><?php echo "R$ ". $this->Custom->numberFormatToBR($item['Agreement']['value']) ?></td>
						<td><?php echo $item['TypeAgreement']['name'] ?></td>
						<td><?php echo $this->Custom->formatDate($item['Agreement']['validity']) ?></td>
						<td><?php echo $item['Agreement']['guarantee'] ?></td>
						<td><?php echo ($item['Agreement']['is_public'] == 1 ? "Sim" : "Não") ?></td>
						<td><?php echo $this->Custom->formatDateWithHours($item['Agreement']['created']) ?></td>
						<td class="text-center fit"><a href="javascript:void(0)<?php ///contratos/editar/ echo $item['Agreement']['id'] ?>" title="Editar"><i class="fa fa-pencil-square-o text-info"></i></a></td>
						<!--<td class="text-center fit"><a href="#" onclick="confirmDeleteItem(<?php //echo $plan['Plan']['id'] ?>)" title="Deletar"><i class="fa fa-trash-o text-danger"></i></a></td>-->
					</tr>
					<?php endforeach; ?>
					<?php unset($result); ?>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php // echo $this->element('sql_dump'); ?>
<?php // echo Configure::version() ?>