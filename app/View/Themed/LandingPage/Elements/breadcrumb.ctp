    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php echo $title_for_layout ?></h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/dashboard/" title="Voltar para a Home">Início</a></li>
                <li><?php echo $this->Html->link($title_for_layout, array('controller' => $this->request->params['controller'], 'action' => 'index'), array('title' => 'Listar todos os itens')); ?></li>
                <li><?php echo $sub_title_for_layout ?></li>
            </ol>
        </div>
    </div>