<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Contratos</h4>
		</div>
		<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			<ol class="breadcrumb">
				<li><a href="/" title="Voltar para a Home">Início</a></li>
				<li><a href="/contratos" title="Listar todos">Contratos</a></li>
			</ol>
		</div>
	</div>
	<!-- /row -->

	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Novo Contrato</h3>
				<p class="text-muted m-b-30">Adição</p>

				<?php echo $this->Form->create('Agreement', array("id" => "agreement_form", "data-toggle" => "validator", "novalidate" => true, "class" => "form-horizontal", "enctype" => "multipart/form-data"));?>

				<div class="form-group">
					<label class="control-label">Nome</label>
					<input name="data[Agreement][name]" type="text" value="" class="form-control" placeholder="Ex.: Compra e Venda de Imóvel Residencial - N. 8B09765IO7UP" required data-error="Este campo é obrigatório" maxlength="120">
				</div>
				<div class="form-group">
					<label class="control-label">CPF/CNPJ</label>
					<input name="data[Agreement][cpf_cnpj]" onkeypress='mascaraMutuario(this,cpfCnpj)' onblur='clearTimeout()' type="text" value="" class="form-control" placeholder="999.999.999-99 ou 99.999.999/9999-99" required data-error="Este campo é obrigatório" maxlength="18">
				</div>
				<div class="form-group">
					<label class="control-label">Valor (R$)</label>
					<input name="data[Agreement][value]" type="text" value="" class="form-control" placeholder="Ex.: 120,50" required data-error="Este campo é obrigatório">
				</div>
				<div class="form-group">
					<label for="textarea" class="control-label">Descrição</label>
					<textarea name="data[Agreement][description]" id="textarea" class="form-control" rows="5" maxlength="500"></textarea>
					<span class="help-block with-errors">Atenção, não ultrapasse 500 caracteres!</span>
				</div>
				<div class="form-group">
					<label class="col-sm-12">Tipo de Contrato</label>
					<select class="form-control" name="data[Agreement][type_agreement_id]" required data-error="Este campo é obrigatório">
						<option value="">Selecione...</option>
						<option value="1">Imóvel</option>
						<option value="2">Automóvel</option>
						<option value="3">Eletrodoméstico</option>
					</select>
				</div>
				<div class="form-group row">
					<label class="col-2 col-form-label">Validade do Contrato</label>
					<div class="col-10">
						<input name="data[Agreement][validity]" class="form-control" value="" required data-error="Este campo é obrigatório" placeholder="dd/mm/aaaa">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label" for="guarantee">Garantia <a title="Texto" data-target="#guarantee_modal" data-toggle="modal"><i class="fa fa-info-circle text-info"></i></a></label>
					<input id="guarantee" name="data[Agreement][guarantee]" type="text" value="" class="form-control" placeholder="" maxlength="200">
				</div>
				<div class="form-group">
					<label class="col-md-12">Arquivo</label>
					<div class="col-md-12">
						<input name="data[Agreement][file_name]" type="file" id="input-file-max-fs" class="dropify" data-max-file-size="5M" />
						<span for="input-file-max-fs" class="help-block with-errors">Apenas formato PDF permitido. Tamanho máximo: 5MB</span>
					</div>
				</div>

				<hr>

				<div class="form-group">
					<div class="col-md-12">
						<div class="checkbox checkbox-success">
							<input id="is_public" type="checkbox" name="data[Agreement][is_public]" value="1">
							<label for="is_public">Contrato Público? <a title="Texto" data-target="#public" data-toggle="modal"><i class="fa fa-info-circle text-info"></i></a></label>
						</div>
					</div>
				</div>

				<hr>

				<button type="submit" class="btn btn-primary">Salvar</button>
				<a href="/contratos" class="btn btn-primary btn-danger">Cancelar</a>

				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>

<div id="guarantee_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Garantia de Contrato Público</h4>
			</div>
			<div class="modal-body text-justify">
				<p>A alienação fiduciária em garantia consiste na transferência feita por um devedor ao credor de propriedade resolúvel e da posse indireta de um bem móvel infungível ou de um bem imóvel, como garantia de seu débito, resolvendo-se o direito do adquirente com o adimplente da obrigação, ou melhor, com o pagamento da dívida garantida.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>

<div id="public" class="modal fade" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Contrato Público</h4>
			</div>
			<div class="modal-body text-justify">
				<p>Um contrato público pode ser encontrado por qualquer pessoa que possua os dados das partes (CPF/CNPJ/Número do Contrato), porém apenas informações básicas serão mostradas.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>

<script>
function mascaraMutuario(o,f)
{
	v_obj=o
	v_fun=f
	setTimeout('execmascara()',1)
}

function execmascara()
{
	v_obj.value=v_fun(v_obj.value)
}

function cpfCnpj(v)
{
	//Remove tudo o que não é dígito
	v=v.replace(/\D/g,"")

	if (v.length <= 14) { //CPF

		//Coloca um ponto entre o terceiro e o quarto dígitos
		v=v.replace(/(\d{3})(\d)/,"$1.$2")

		//Coloca um ponto entre o terceiro e o quarto dígitos
		//de novo (para o segundo bloco de números)
		v=v.replace(/(\d{3})(\d)/,"$1.$2")

		//Coloca um hífen entre o terceiro e o quarto dígitos
		v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2")

	} else { //CNPJ

		//Coloca ponto entre o segundo e o terceiro dígitos
		v=v.replace(/^(\d{2})(\d)/,"$1.$2")

		//Coloca ponto entre o quinto e o sexto dígitos
		v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3")

		//Coloca uma barra entre o oitavo e o nono dígitos
		v=v.replace(/\.(\d{3})(\d)/,".$1/$2")

		//Coloca um hífen depois do bloco de quatro dígitos
		v=v.replace(/(\d{4})(\d)/,"$1-$2")

	}
	return v
}
</script>