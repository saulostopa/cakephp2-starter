<?php

class PagseguroComponent extends Component
{

    public function add_plan($data)
    {
        $email = Configure::read('PAGSEGURO_EMAIL');
        $token = Configure::read('PAGSEGURO_TOKEN');

        $url = "https://".Configure::read('PAGSEGURO_URL_TRANSACTION_SIGNATURE')."/pre-approvals/request?email=$email&token=$token";
        $data_request = array
        (
            //"redirectURL"  => ""
             "reference"    => $data['Plan']['reference']
            ,"preApproval"  => array
            (
                 "name"                     => $data['Plan']['name']
                ,"charge"                   => "AUTO"
                ,"period"                   => "MONTHLY"
                ,"amountPerPayment"         => $data['Plan']['value']
                /*,"membershipFee"            => 150
                ,"trialPeriodDuration"      => 28
                ,"details"                  => "string"
                ,"maxAmountPerPeriod"       => 100
                ,"maxAmountPerPayment"      => 100
                ,"maxTotalAmount"           => 100
                ,"maxPaymentsPerPeriod"     => 3
                ,"initialDate"              => "2017-08-31"
                ,"finalDate"                => "2017-08-31"
                ,"dayOfYear"                => "03-27"
                ,"dayOfMonth"               => 1
                ,"dayOfWeek"                => "MONDAY"
                ,"cancelURL"                => ""*/
                ,"expiration"               => array
                (
                     "value"                => 12
                    ,"unit"                 => "MONTHS"
                )
            )
            /*,"reviewURL"                    => "http://lojamodelo.com.br/revisar"
            ,"maxUses"                      => 500
            ,"receiver"                     => array
            (
                "email"                     => "suporte@lojamodelo.com.br"
            )*/
        );

        $ch = curl_init();

        $data_string = json_encode($data_request);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_HEADER, 1); // uncomment this line just for debug
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json;charset=ISO-8859-1', 'Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1', 'Content-Length: ' . strlen($data_string)));

        $result = curl_exec($ch);
        return $result;
    }

    public function edit_plan($data)
    {

        $email = Configure::read('PAGSEGURO_EMAIL');
        $token = Configure::read('PAGSEGURO_TOKEN');

        $url = "https://".Configure::read('PAGSEGURO_URL_TRANSACTION_SIGNATURE')."/pre-approvals/request/".$data['Plan']['code_pagseguro']."/payment?email=$email&token=$token";
        $data_request = array
        (
             "amountPerPayment"    => $data['Plan']['value']
            ,"updateSubscriptions" => false
        );

        $ch = curl_init();

        $data_string = json_encode($data_request);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_HEADER, 1); // uncomment this line just for debug
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json;charset=ISO-8859-1', 'Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1', 'Content-Length: ' . strlen($data_string)));

        $result = curl_exec($ch);
        return $result;
    }
}