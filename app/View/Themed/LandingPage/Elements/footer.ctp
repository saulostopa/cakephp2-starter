    <!-- FOOTER -->
    <footer id="footer">
        <div class="container">

            <div class="row">

                <!-- col #1 -->
                <div class="logo_footer dark col-md-3">

                    <img alt="" src="/theme/LandingPage/assets/images/logo_footer.png" height="50" class="logo" />

                    <p class="block">
                        4th Street, Suite 88<br />
                        New York NY 10887<br />
                        Email: youremail@yourdomain.com<br />
                        Phone: +40 (0) 111 1234 567<br />
                        Fax: +40 (0) 111 1234 568<br />
                    </p>

                    <p class="block"><!-- social -->
                        <a href="#" class="social fa fa-facebook"></a>
                        <a href="#" class="social fa fa-twitter"></a>
                        <a href="#" class="social fa fa-google-plus"></a>
                        <a href="#" class="social fa fa-linkedin"></a>
                    </p><!-- /social -->
                </div>
                <!-- /col #1 -->

                <!-- col #2 -->
                <div class="spaced col-md-3 col-sm-4 hidden-xs">
                    <h4>Recent <strong>Posts</strong></h4>
                    <ul class="list-unstyled">
                        <li>
                            <a class="block" href="#">New CSS3 Transitions this Year</a>
                            <small>June 29, 2014</small>
                        </li>
                        <li>
                            <a class="block" href="#">Inteligent Transitions In UX Design</a>
                            <small>June 29, 2014</small>
                        </li>
                        <li>
                            <a class="block" href="#">Lorem Ipsum Dolor</a>
                            <small>June 29, 2014</small>
                        </li>
                    </ul>
                </div>
                <!-- /col #2 -->

                <!-- col #3 -->
                <div class="spaced col-md-3 col-sm-4 hidden-xs">
                    <h4>Recent <strong>Tweets</strong></h4>
                    <ul class="list-unstyled fsize13">
                        <li>
                            <i class="fa fa-twitter"></i> <a href="#">@John Doe</a> Pilsum dolor lorem sit consectetur adipiscing orem sequat <small class="ago">8 mins ago</small>
                        </li>
                        <li>
                            <i class="fa fa-twitter"></i> <a href="#">@John Doe</a> Remonde sequat ipsum dolor lorem sit consectetur adipiscing  <small class="ago">8 mins ago</small>
                        </li>
                        <li>
                            <i class="fa fa-twitter"></i> <a href="#">@John Doe</a> Imperdiet condimentum diam dolor lorem sit consectetur adipiscing <small class="ago">8 mins ago</small>
                        </li>
                    </ul>
                </div>
                <!-- /col #3 -->

                <!-- col #4 -->
                <div class="spaced col-md-3 col-sm-4">
                    <h4>About <strong>Us</strong></h4>
                    <p>
                        Incredibly beautiful responsive Bootstrap Template for Corporate and Creative Professionals.
                    </p>

                    <h4><small><strong>Subscribe to our Newsletter</strong></small></h4>
                    <form id="newsletterSubscribe" method="post" action="php/newsletter.php" class="input-group">
                        <input required type="email" class="form-control" name="newsletter_email" id="newsletter_email" value="" placeholder="E-mail Address">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary">SUBMIT</button>
                                    </span>
                    </form>

                </div>
                <!-- /col #4 -->

            </div>

        </div>

        <hr />

        <div class="copyright">
            <div class="container text-center fsize12">
                Epona theme by <a href="http://www.stepofweb.com" target="_blank" title="bootstrap themes &amp; templates" class="copyright">stepofweb</a> &bull; All Right Reserved &copy; Your Company LLC. &nbsp;
                <a href="page-privacy.html" class="fsize11">Privacy Policy</a> &bull;
                <a href="page-terms.html" class="fsize11">Terms of Service</a>
            </div>
        </div>
    </footer>
    <!-- /FOOTER -->

    <a href="#" id="toTop"></a>