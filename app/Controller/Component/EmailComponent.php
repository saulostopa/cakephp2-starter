<?php
App::uses('CakeEmail', 'Network/Email');

class EmailComponent extends Component
{
    function sendMail($data = array())
    {
        $Email = new CakeEmail('default');
        $Email->config();
        $Email->from( $data["email_from"] );
        $Email->to( $data["to_email"] );
        $Email->emailFormat('both');
        $Email->subject($data["subject"]);

        $content['subject']             = $data["subject"];
        $content['to_name']             = $data["to_name"];
        $content['welcome']             = Configure::read('BRAND');
        $content['intro']               = Configure::read('BRAND_SLOGAN');
        $content['body']                = $data["body"];
        $content['from_first_name']     = $data["from_first_name"];
        $content['from_last_name']      = $data["from_last_name"];
        $content['from_email']          = $data['from_email'];

        $content['is_acknowledgment_receipt'] = $data['is_acknowledgment_receipt'];
        $content['is_electronic_signature']   = $data['is_electronic_signature'];

        if ( !empty($data["is_acknowledgment_receipt"]) && $data["is_acknowledgment_receipt"] )
        {
            $content['is_acknowledgment_receipt'] = true;
            $content['link']                      = Configure::read('HOST') . "/notificacoes/token/" . $data["token"];
        }

        if ( !empty($data["is_electronic_signature"]) && $data["is_electronic_signature"] )
        {
            $content['is_electronic_signature']   = true;
            $content['link']                      = Configure::read('HOST') . "/notificacoes/assinatura-eletronica/token/" . $data["token"];
        }

        $Email->template('default','layout_default'); // Emails/html/default.ctp | Layouts/Emails/html/layout_default.ctp
        $Email->emailFormat('html');
        $Email->viewVars($content);

        if ( ! $Email->send() ) return false;

        return true;
    }
}