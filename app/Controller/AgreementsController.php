<?php

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('Text', 'Utility');
App::uses('CakeEmail', 	'Network/Email');
App::uses('AppController', 'Controller');

class AgreementsController extends AppController {

	public $helpers 		= array('Html','Form','Flash','Custom');
	public $components 		= array('Flash','Session','Paginator','Email','Utility','Date','Validation');
	public $uses 			= array();

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('search_agreements','search_agreements_view','search_agreements_pay');
	}

	public function index()
	{
		$this->set('title_for_layout', 'Contratos');
		$this->Paginator->settings = $this->paginate;

		$conditions = [];
		if ( AuthComponent::user('role') != 'admin' )
		{
			$conditions['Agreement.user_id'] = AuthComponent::user('id');
		}

		$this->Paginator->settings = array
		(
			 'limit'        => 1000
			,'order' 		=> array('Agreement.created' => 'desc')
			,'conditions'   => $conditions
			//,'recursive'    => 1
		);

		$result					   = $this->Paginator->paginate('Agreement');

		$numbers = count($result);
		$this->set(compact('result', 'numbers'));
	}

	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->request->data['Agreement']['value'] = $this->Utility->numberFormatToUS($this->request->data['Agreement']['value']);

			$this->request->data['Agreement']['user_id'] = $this->Auth->user('id');

			$cpf_cnpj = $this->request->data['Agreement']['cpf_cnpj'];
			if ( strlen($cpf_cnpj) == 14 ) {
				$cpf_cnpj = $this->Validation->cpf_validation( $cpf_cnpj );
				if ( $cpf_cnpj == false )
					return $this->Flash->error(__("CPF Inválido"));
			}
			else
			{
				$cpf_cnpj = $this->Validation->cnpj_validation( $cpf_cnpj );
				if ( $cpf_cnpj == false )
					return $this->Flash->error(__("CNPJ Inválido"));
			}


			if ( empty($this->request->data['Agreement']['file_name']["name"]))
			{
				unset($this->request->data['Agreement']['file_name']);
			}
			else
			{
				$allowedExts = array("pdf");
				$extension = pathinfo($this->request->data['Agreement']['file_name']["name"], PATHINFO_EXTENSION);

				if ( $this->request->data['Agreement']['file_name']["type"] == "application/pdf" && $this->request->data['Agreement']['file_name']['size'] < 5000000 && in_array($extension, $allowedExts) )
				{
					if ($this->request->data['Agreement']['file_name']["error"] > 0) return $this->Flash->error(__("Erro 459, tente novamente ou contacte o admnistrador!"));
				}
				else
				{
					return $this->Flash->error(__("Erro, extensão não permitida! Ou tamanho maior que suportado."));
				}
			}

			if ( ! $this->Date->formatDate($this->request->data['Agreement']['validity']) )
			{
				return $this->Flash->error(__('Erro 110 - Data de validade informada incorreta'));
			}

			$this->request->data['Agreement']['validity'] = $this->Date->formatDate($this->request->data['Agreement']['validity']);

			$this->request->data['Agreement']['cpf_cnpj'] = preg_replace('/[^0-9]/', '',$this->request->data['Agreement']['cpf_cnpj']);

			$this->Agreement->create();

			if ($this->Agreement->save($this->request->data))
			{
				$this->Flash->success(__('Dados salvo com sucesso'));
				return $this->redirect(array('action' => 'index'));
			}
			$this->Flash->error(__('Erro 109 - Problemas para salvar seus dados. Tente novamente ou contacte o administrador.'));

		}
	}

	public function view(){}
	public function del(){}

	public function search_agreements()
	{
		$this->layout = 'agreement_public';

		if ($this->request->is('post')) {

			$search_term = $this->request->data['Agreement']['search_term'];

			$this->Paginator->settings = $this->paginate;
			$conditions = array();
			$limit = 20;

			$conditions['Agreement.is_public'] = 1;

			if (!empty($search_term) && !empty($search_term)) {
				$conditions['Agreement.cpf_cnpj'] = $search_term;
				$limit = 20;
			}

			$this->Paginator->settings = array(
				 'order' => array('Agreement.date' => 'desc')
				, 'limit' => $limit
				, 'conditions' => $conditions
			);

			$result = $this->Paginator->paginate('Agreement');

			$search_total_result = count($result);
			$this->set(compact('result','search_term', 'search_total_result'));
		}
	}

	public function search_agreements_view($id = null)
	{
		$this->layout = 'agreement_public';

		$this->Agreement->id = $id;
		if (!$this->Agreement->exists()) {
			throw new NotFoundException(__('Requisição Inválida'));
		}
		$this->set('agreement', $this->Agreement->findById($id));
	}

	public function search_agreements_pay($id = null)
	{
		$this->layout = 'agreement_public';

		$this->Agreement->id = $id;
		if (!$this->Agreement->exists()) {
			throw new NotFoundException(__('Requisição Inválida'));
		}

		if ($this->request->is('post')) {

		}

		$agreement = $this->Agreement->findById($id);
		$plans = array();
		$user = array('User');

		$this->set(compact('agreement','plans','user'));
	}

}