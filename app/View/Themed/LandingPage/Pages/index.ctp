    <!-- REVOLUTION SLIDER -->
    <div class="onepage-slider-offset">

        <div class="slider fullwidthbanner-container roundedcorners">
            <div class="fullscreenbanner tp-simpleresponsive">
                <ul class="hide">

                    <!-- SLIDE  -->
                    <li data-transition="boxfade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"  data-title="Slide">

                        <img src="/theme/LandingPage/assets/images/1x1.png" data-lazyload="/theme/LandingPage/assets/images/demo/blue.jpg" alt="" data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat" />

                        <div class="tp-caption very_large_text lfb ltt tp-resizeme"
                             data-x="center" data-hoffset="0"
                             data-y="center" data-voffset="-100"
                             data-speed="600"
                             data-start="800"
                             data-easing="Power4.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn">THINK BIG
                        </div>

                        <div class="tp-caption medium_light_white lfb ltt tp-resizeme"
                             data-x="center" data-hoffset="0"
                             data-y="center" data-voffset="10"
                             data-speed="600"
                             data-start="900"
                             data-easing="Power4.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn">Happiness is an accident of nature,<br/>a beautiful and flawless aberration.<br/><span style="font-size:24px;font-weight:400;">&ndash; Pat Conroy</span>
                        </div>

                    </li>

                    <!-- SLIDE  -->
                    <li data-transition="boxfade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"  data-title="Slide">

                        <img src="/theme/LandingPage/assets/images/1x1.png" data-lazyload="/theme/LandingPage/assets/images/demo/blur.jpg" alt="" data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat" />

                        <div class="tp-caption mediumlarge_light_white lfb tp-resizeme"
                             data-x="left" data-hoffset="70"
                             data-y="133"
                             data-speed="1000"
                             data-start="1400"
                             data-easing="easeOutExpo"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-endspeed="300">+ Amazing Layouts
                        </div>

                        <div class="tp-caption mediumlarge_light_white lft tp-resizeme"
                             data-x="left" data-hoffset="70"
                             data-y="85"
                             data-speed="1000"
                             data-start="1200"
                             data-easing="easeOutExpo"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-endspeed="300">+ Awesome  Sliders
                        </div>

                        <div class="tp-caption block_black sfl tp-resizeme"
                             data-x="70"
                             data-y="216"
                             data-speed="750"
                             data-start="1900"
                             data-easing="easeOutExpo"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-endspeed="300">Premium Sliders That Are Easy To Use With Any Content
                        </div>

                        <div class="tp-caption block_black sfr tp-resizeme"
                             data-x="70"
                             data-y="260"
                             data-speed="750"
                             data-start="2200"
                             data-easing="easeOutExpo"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-endspeed="300">Loaded With Options, And Is Simply A Joy To Use
                        </div>

                    </li>

                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>

    </div><!-- /topnav offset -->
    <!-- /REVOLUTION SLIDER -->


    <!-- CALLOUT -->
    <div class="callout dark"><!-- add "styleBackgroundColor" class for colored background and white text OR "dark" for a dark callout -->
        <div class="container">

            <div class="row">

                <div class="col-md-9"><!-- title + shortdesc -->
                    <h3>The best way to grow your business on the internet</h3>
                    <p>Check Epona included features and options.</p>
                </div>

                <div class="col-md-3"><!-- button -->
                    <a href="#purchase" class="btn btn-primary btn-lg">Get Started Now</a>
                </div>

            </div>

        </div>
    </div>
    <!-- /CALLOUT -->


    <!-- ABOUT -->
    <section id="about">
        <div class="container">

            <header>
                <h2>
                    Welcome to
                                <span class="word-rotator"><!-- word rotator -->
                                    <span class="items bold">
                                        <span><em>Epona</em></span>
                                        <span><em>the future</em></span>
                                        <span><em>multipurpose</em></span>
                                    </span>
                                </span><!-- /word rotator -->
                </h2>

                <div class="divider half-margins onepage"><!-- lines divider --></div>

            </header>


            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque. Ut enim massa, sodales tempor convallis et.</p>

            <hr class="half-margins invisible" /><!-- 60px spacer -->

            <div class="row text-left">

                <!-- Passion -->
                <div class="col-md-4">
                    <h3>Passion</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque. Ut enim massa, sodales tempor convallis et, iaculis ac massa. Etiam suscipit nisl eget lorem pellentesque quis iaculis mi mattis.</p>
                </div>


                <!-- Precision -->
                <div class="col-md-4">
                    <h3>Precision</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque. Ut enim massa, sodales tempor convallis et, iaculis ac massa. Etiam suscipit nisl eget lorem pellentesque quis iaculis mi mattis.</p>
                </div>


                <!-- Simplicity -->
                <div class="col-md-4">
                    <h3>Simplicity</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque. Ut enim massa, sodales tempor convallis et, iaculis ac massa. Etiam suscipit nisl eget lorem pellentesque quis iaculis mi mattis.</p>
                </div>

            </div>

        </div>
    </section>
    <!-- /ABOUT -->


    <!-- PORTFOLIO -->
    <section id="work" class="alternate">
        <div class="container">

            <header class="text-center">
                <h1 class="font-dosis">
                    <strong>OUR</strong> WORK
                </h1>

                <div class="divider half-margins onepage center"><!-- lines divider --></div>

            </header>

            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque. Ut enim massa, sodales tempor convallis et.</p>


            <div id="portfolio">

                <ul class="nav nav-pills isotope-filter isotope-filter noborder" data-sort-id="isotope-list" data-option-key="filter">
                    <li data-option-value="*" class="active"><a href="#">Show All</a></li>
                    <li data-option-value=".development"><a href="#">Development</a></li>
                    <li data-option-value=".photography"><a href="#">Photography</a></li>
                    <li data-option-value=".design"><a href="#">Design</a></li>
                </ul>


                <div class="row">

                    <ul class="sort-destination isotope" data-sort-id="isotope-list">

                        <li class="sotope-item col-sm-6 col-md-4 development"><!-- item -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover" href="portfolio-single-basic.html">
                                        <span class="overlay color2"></span>
                                                    <span class="inner">
                                                        <span class="block fa fa-plus fsize20"></span>
                                                        <strong>PROJECT</strong> DETAIL
                                                    </span>
                                    </a>
                                    <img class="img-responsive" src="/theme/LandingPage/assets/images/demo/portfolio/a1.jpg" width="260" height="260" alt="">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>Epona Project</h4>
                                    <small class="styleColor">29 June, 2014</small>
                                </div>
                            </div>
                        </li>

                        <li class="sotope-item col-sm-6 col-md-4 photography"><!-- item 2 -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover lightbox" href="http://www.youtube.com/watch?v=W7Las-MJnJo" data-plugin-options='{"type":"iframe"}'>
                                        <span class="overlay color2"></span>
                                                    <span class="inner">
                                                        <span class="block fa fa-plus fsize20"></span>
                                                        <strong>VIEW</strong> VIDEO
                                                    </span>
                                    </a>
                                    <img class="img-responsive" src="/theme/LandingPage/assets/images/demo/portfolio/a2.jpg" width="260" height="260" alt="">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>Video Project</h4>
                                    <small class="styleColor">29 June, 2014</small>
                                </div>
                            </div>
                        </li>

                        <li class="sotope-item col-sm-6 col-md-4 design"><!-- item 3 -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover" href="portfolio-single-basic.html">
                                        <span class="overlay color2"></span>
                                                    <span class="inner">
                                                        <span class="block fa fa-plus fsize20"></span>
                                                        <strong>PROJECT</strong> DETAIL
                                                    </span>
                                    </a>
                                    <img class="img-responsive" src="/theme/LandingPage/assets/images/demo/portfolio/a3.jpg" width="260" height="260" alt="">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>Epona Project</h4>
                                    <small class="styleColor">29 June, 2014</small>
                                </div>
                            </div>
                        </li>

                        <li class="sotope-item col-sm-6 col-md-4 photography"><!-- item 4 -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover" href="portfolio-single-basic.html">
                                        <span class="overlay color2"></span>
                                                    <span class="inner">
                                                        <span class="block fa fa-plus fsize20"></span>
                                                        <strong>PROJECT</strong> DETAIL
                                                    </span>
                                    </a>
                                    <img class="img-responsive" src="/theme/LandingPage/assets/images/demo/portfolio/a4.jpg" width="260" height="260" alt="">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>Epona Project</h4>
                                    <small class="styleColor">29 June, 2014</small>
                                </div>
                            </div>
                        </li>

                        <li class="sotope-item col-sm-6 col-md-4 development"><!-- item 5 -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover lightbox" href="http://www.youtube.com/watch?v=W7Las-MJnJo" data-plugin-options='{"type":"iframe"}'>
                                        <span class="overlay color2"></span>
                                                    <span class="inner">
                                                        <span class="block fa fa-plus fsize20"></span>
                                                        <strong>VIEW</strong> VIDEO
                                                    </span>
                                    </a>
                                    <img class="img-responsive" src="/theme/LandingPage/assets/images/demo/portfolio/a5.jpg" width="260" height="260" alt="">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>Video Project</h4>
                                    <small class="styleColor">29 June, 2014</small>
                                </div>
                            </div>
                        </li>

                        <li class="sotope-item col-sm-6 col-md-4 design"><!-- item 6 -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover" href="portfolio-single-basic.html">
                                        <span class="overlay color2"></span>
                                                    <span class="inner">
                                                        <span class="block fa fa-plus fsize20"></span>
                                                        <strong>PROJECT</strong> DETAIL
                                                    </span>
                                    </a>
                                    <img class="img-responsive" src="/theme/LandingPage/assets/images/demo/portfolio/a6.jpg" width="260" height="260" alt="">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>Epona Project</h4>
                                    <small class="styleColor">29 June, 2014</small>
                                </div>
                            </div>
                        </li>

                    </ul>

                </div><!-- /.masonry-container -->

                <!-- CALLOUT -->
                <div class="alert alert-default fade in">

                    <div class="row">

                        <div class="col-md-9 col-sm-9"><!-- left text -->
                            <h4>Purchase <strong>Epona Template</strong></h4>
                            <p>
                                We truly care about our users and our product. Which is why we are constantly improving Epona for our beloved users.
                            </p>
                        </div><!-- /left text -->


                        <div class="col-md-3 col-sm-3 text-right"><!-- right btn -->
                            <a href="#purchase" rel="nofollow" target="_blank" class="btn btn-primary btn-lg">PURCHASE NOW</a>
                        </div><!-- /right btn -->

                    </div>

                </div>
                <!-- /CALLOUT -->

            </div>


        </div>
    </section>
    <!-- /PORTFOLIO -->


    <!-- parallax -->
    <div class="parallax parallax-1" style="background-image:url(/theme/LandingPage/assets/images/demo/other/o2.jpg);">
        <span class="parallax-overlay"></span>

        <div class="container parallax-content">

            <div class="row">
                <div class="col-md-5 col-sm-5">

                    <!-- testimonial -->
                    <div class="owl-carousel text-center fsize20" data-plugin-options='{"items": 1, "singleItem": true, "navigation": false, "pagination": true, "autoPlay": true}'>

                        <div>
                            <p>This is the Best Theme I have ever seen. I am a full time web dev, and this theme blows me away. I wanted to thank you for all your hard work. I am really looking forward to building my new site! I think you thought of everything and then some.</p>
                            <cite><strong>John Doe</strong>, Customer</cite>
                        </div>

                        <div>
                            <p>Donec tellus massa, tristique sit amet condim vel, facilisis quis sapien. Praesent id enim sit amet odio vulputate eleifend in in tortor. Donec tellus massa.</p>
                            <cite><strong>Jessica Doe</strong>, Customer</cite>
                        </div>

                        <div>
                            <p>Praesent est laborum dolo rumes fugats untras. Etha rums ser quidem rerum facilis dolores nemis onis fugats vitaes nemo minima rerums unsers sadips amets.</p>
                            <cite><strong>Dorin Doe</strong>, Customer</cite>
                        </div>

                    </div>
                    <!-- /testimonial -->

                </div>

                <div class="col-md-7 col-sm-7 text-right">


                    <h4 class="nomargin">Give us a call</h4>
                    <h1>800 - 1234 567</h1>

                </div>
            </div>

        </div>

    </div>
    <!-- /parallax -->


    <!-- SERVICES -->
    <section id="services">
        <div class="container">

            <header class="text-center">

                <h1 class="font-dosis">
                    <strong>OUR</strong> SERVICES
                </h1>

                <div class="divider half-margins onepage center"><!-- lines divider --></div>

            </header>

            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque. Ut enim massa, sodales tempor convallis et.</p>

            <hr class="half-margins invisible" /><!-- 60px spacer -->

            <div class="row">

                <div class="col-md-4">

                    <figure>
                        <img class="img-responsive" src="/theme/LandingPage/assets/images/demo/other/o1.jpg" alt="" />
                    </figure>

                    <h4>Responsive</h4>
                    <p>Epona is fully responsive and can adapt to any screen size, its incredibly fast and flexible. Try resizing your browser window to see the action.</p>

                </div>

                <div class="col-md-4">

                    <figure>
                        <img class="img-responsive" src="/theme/LandingPage/assets/images/demo/other/o3.jpg" alt="" />
                    </figure>

                    <h4>Endless Possibilities</h4>
                    <p>You have endless possibilities to create layouts - various shortcodes and page layouts!</p>

                </div>

                <div class="col-md-4">

                    <figure>
                        <img class="img-responsive" src="/theme/LandingPage/assets/images/demo/other/o4.jpg" alt="" />
                    </figure>

                    <h4>Powerful Sliders</h4>
                    <p>Epona includes the Revolution Slider, Layer Slider, Flex Slider and the OWL Carousel to help your content stand out to your viewer.</p>

                </div>

            </div>


            <hr />


            <!-- FEATURED BOXES 3 -->
            <div class="row featured-box-minimal">

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h4><i class="fa fa-group"></i> Customer Support</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Lorem ipsum dolor sit amet, consectetur adipiscing metus elit.</p>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h4><i class="fa fa-code"></i> HTML5 / CSS3</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Lorem ipsum dolor sit amet, consectetur adipiscing metus elit.</p>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h4><i class="fa fa-bars"></i> Buttons</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Lorem ipsum dolor sit amet, consectetur adipiscing metus elit.</p>
                </div>


                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h4><i class="fa fa-google-plus"></i> Google Fonts</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Lorem ipsum dolor sit amet, consectetur adipiscing metus elit.</p>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h4><i class="fa fa-check"></i> Icons</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Lorem ipsum dolor sit amet, consectetur adipiscing metus elit.</p>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h4><i class="fa fa-desktop"></i> Lightbox</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Lorem ipsum dolor sit amet, consectetur adipiscing metus elit.</p>
                </div>

            </div>
            <!-- /FEATURED BOXES 3 -->

        </div>
    </section>
    <!-- /SERVICES -->


    <!-- CONTACT -->
    <section id="contact" class="alternate">
        <div class="container">

            <header class="text-center">

                <h1 class="font-dosis">
                    CONTACT
                </h1>

                <div class="divider half-margins onepage center"><!-- lines divider --></div>

            </header>

            <div class="text-center">
                <h4>Drop us a line or <strong>just say</strong> <em><strong>Hello!</strong></em></h4>
                <p>
                    Lorem ipsum dolor sit amet, <strong>consectetur adipiscing</strong> elit. <br />
                    Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc.
                </p>
            </div>

            <!-- Form -->
            <form id="onepageContact" method="post" action="#" class="onepage block form-inline margin-top60">
                <div class="row">
                    <div class="col-md-4"><input required class="fullwidth" type="text" name="contact_name" id="contact_name" value="" placeholder="NAME *" title="Name" /></div>
                    <div class="col-md-4"><input required class="fullwidth" type="email" name="newsletter_email" id="newsletter_email" value="" placeholder="EMAIL *" title="Email" /></div>
                    <div class="col-md-4"><input class="fullwidth" type="text" name="contact_subject" id="contact_subject" value="" placeholder="SUBJECT" title="subject" /></div>
                </div>

                <div class="row">
                    <div class="col-md-12"><textarea required class="fullwidth" rows="5" name="contact_comment" id="contact_comment" placeholder="MESSAGE *"></textarea></div>
                    <div class="col-md-12">
                        <button id="contact_submit" class="btn btn-primary">SEND MESSAGE</button>
                    </div>
                </div>
            </form>


        </div>
    </section>
    <!-- /CONTACT -->