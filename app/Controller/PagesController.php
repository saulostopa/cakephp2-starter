<?php

App::uses('AppController', 'Controller');

class PagesController extends AppController {

	public $uses = array();
	public $components = array("Session","RequestHandler","Paginator");
	public $helpers = array ('Html','Form', 'Js');

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->deny();
		$this->Auth->allow('index');
	}

	public function index()
	{
		$this->theme = 'LandingPage';
		$this->set('title_for_layout', 'Home');
	}

	public function dashboard()
	{
		$this->set('title_for_layout', 'Dashboard');
	}

	public function cakepage()
	{
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		if (in_array('..', $path, true) || in_array('.', $path, true)) {
			throw new ForbiddenException();
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
}
