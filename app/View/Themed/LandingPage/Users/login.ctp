<section class="page-title">
    <div class="container">

        <header>

            <ul class="breadcrumb"><!-- breadcrumb -->
                <li><a href="#">Pages</a></li>
                <li class="active">Login Page</li>
            </ul><!-- /breadcrumb -->

            <h2><!-- Page Title -->
                Login
            </h2><!-- /Page Title -->

        </header>

    </div>
</section>

<section>
    <div class="container">

        <div class="row">

            <!-- LOGIN -->
            <div class="col-md-6 col-sm-6">

                <!-- login form -->
                <form action="/login" method="post" class="sky-form boxed">
                    <header>I'm a returning customer</header>
                    <fieldset>
                        <section>
                            <label class="input">
                                <i class="icon-append fa fa-envelope"></i>
                                <input required="" name="data[User][email]" type="email" placeholder="Email address">
                                <b class="tooltip tooltip-bottom-right">Needed to verify your account</b>
                            </label>
                        </section>

                        <section>
                            <label class="input">
                                <i class="icon-append fa fa-lock"></i>
                                <input required="" name="data[User][password]" type="password" placeholder="Password">
                                <b class="tooltip tooltip-bottom-right">Only latin characters and numbers</b>
                            </label>
                            <div class="note"><a href="#">Forgot Password?</a></div>
                        </section>

                        <section>
                            <label class="checkbox"><input type="checkbox" name="checkbox-inline"><i></i>Keep me logged in</label>
                        </section>

                    </fieldset>
                    <footer>
                        <button type="submit" class="button">Log in</button>
                    </footer>
                </form>
                <!-- /login form -->

            </div>
            <!-- /LOGIN -->

            <!-- REGISTER -->
            <div class="col-md-6 col-sm-6">

                <!-- registration form -->
                <form action="#" method="post" class="sky-form boxed">
                    <header>Register an account</header>

                    <fieldset>
                        <section>
                            <label class="input">
                                <i class="icon-append fa fa-envelope"></i>
                                <input required="" type="email" placeholder="Email address">
                                <b class="tooltip tooltip-bottom-right">Needed to verify your account</b>
                            </label>
                        </section>

                        <section>
                            <label class="input">
                                <i class="icon-append fa fa-lock"></i>
                                <input required="" type="password" placeholder="Password">
                                <b class="tooltip tooltip-bottom-right">Only latin characters and numbers</b>
                            </label>
                        </section>

                        <section>
                            <label class="input">
                                <i class="icon-append fa fa-lock"></i>
                                <input required="" type="password" placeholder="Confirm password">
                                <b class="tooltip tooltip-bottom-right">Only latin characters and numbers</b>
                            </label>
                        </section>
                    </fieldset>

                    <fieldset>
                        <div class="row">
                            <section class="col col-md-6">
                                <label class="input">
                                    <input required="" type="text" placeholder="First name">
                                </label>
                            </section>
                            <section class="col col-md-6">
                                <label class="input">
                                    <input required="" type="text" placeholder="Last name">
                                </label>
                            </section>
                        </div>

                        <section>
                            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>I agree to the Terms of Service</label>
                            <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>I want to receive news and  special offers</label>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="button">Submit</button>
                    </footer>
                </form>
                <!-- /registration form -->

            </div>
            <!-- /REGISTER -->

        </div>

    </div>
</section>